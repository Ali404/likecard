﻿using GameFreelancApp.GameContext;
using GameFreelanceApp.Models;

namespace GameFreelanceApp.BackgroundJobs
{
    public class ResetScoresJob : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly ILogger _logger;

        public ResetScoresJob(IServiceProvider serviceProvider,
            IWebHostEnvironment webHostEnvironment,
            IConfiguration configuration,
            ILogger<ResetScoresJob> logger)
        {
            _serviceProvider = serviceProvider;
            _webHostEnvironment = webHostEnvironment;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                if (DateTime.Now.DayOfWeek == DayOfWeek.Thursday && DateTime.Now.TimeOfDay == new TimeSpan(23, 59, 59))
                {
                    _logger.LogInformation("Starting job");
                    using (var scope = _serviceProvider.CreateScope())
                    {
                        _logger.LogInformation("");
                        var dbContext = scope.ServiceProvider.GetService<GameDbContext>();

                        var data = dbContext.UsersAttempts.Where(x => DateTime.Now.Date.Subtract(x.Date).Days <= 7).GroupBy(x => x.Id)
                                                            .ToDictionary(c => c.Key, val => val.Sum(x => x.Points)).OrderByDescending(x => x.Value).Take(3);

                        var toBeInserted = new List<TopUsers>();
                        foreach (var item in data)
                        {

                            toBeInserted.Add(new TopUsers
                            {
                                UserId = item.Key,
                                Date = DateTime.Now
                            });
                            ;
                        }
                        await dbContext.AddRangeAsync(toBeInserted);
                    }

                }
                await Task.Delay(60 * 60 * 24);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, "");
            }
        }
    }

}

