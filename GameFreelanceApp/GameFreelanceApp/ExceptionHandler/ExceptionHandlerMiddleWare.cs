﻿using GameFreelanc.Response;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameFreelancApp.ExceptionHandler
{
    public class ExceptionHandlerMiddleWare
    {
        private readonly RequestDelegate _next;

        private readonly IWebHostEnvironment _env;

        public ILogger Logger { get; }

        public ExceptionHandlerMiddleWare(RequestDelegate next, IWebHostEnvironment env, ILogger<ExceptionHandlerMiddleWare> Logger)
        {
            this._next = next;
            this._env = env;
            this.Logger = Logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch (ApplicationException ex)
            {
                Logger.LogCritical(ex?.Message + ex?.StackTrace);
                //error response
                var result = new BaseAPIResult
                {
                    IsSuccess = false,
                    Message = ex?.Message ?? "Un expected error !!"
                };
                context.Response.OnStarting((state) =>
                {
                    context.Response.StatusCode = 500;
                    context.Response.ContentType = "application/json";
                    return Task.FromResult(0);
                }, null);
                var serializerSettings = new JsonSerializerSettings();
                serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                await context.Response.WriteAsync(JsonConvert.SerializeObject(result, serializerSettings));
                await Task.CompletedTask;
            }
            catch (Exception ex)
            {
                if (this._env.EnvironmentName == Environments.Development)
                    throw;
                Logger?.LogCritical(ex?.Message + ex?.StackTrace);
                //error response
                //toDo: remove the exception message
                var result = new BaseAPIResult
                {
                    IsSuccess = false,
                    Message = ex?.Message + ex?.InnerException?.Message + ex?.StackTrace ?? (this._env.EnvironmentName == Environments.Development ? ex.Message + ex.StackTrace : "Un expected error")
                };
                context.Response.OnStarting((state) =>
                {
                    context.Response.StatusCode = 500;
                    context.Response.ContentType = "application/json";
                    return Task.FromResult(0);
                }, null);

                var serializerSettings = new JsonSerializerSettings();
                serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                await context.Response.WriteAsync(JsonConvert.SerializeObject(result, serializerSettings));
                await Task.CompletedTask;
            }
        }
    }
}
