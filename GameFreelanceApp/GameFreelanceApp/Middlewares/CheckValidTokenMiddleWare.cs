﻿using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace GameFreelanceApp.Middlewares
{
    public class CheckValidTokenMiddleWare
    {
        private readonly RequestDelegate _next;
        private readonly IServiceProvider _provider;
        public IConfiguration Configuration { get; set; }

        public CheckValidTokenMiddleWare(RequestDelegate next, IServiceProvider provider, IConfiguration configuration)
        {

            _next = next;
            _provider = provider;
            Configuration = configuration;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            if (httpContext.Request.Headers.ContainsKey("Authorization"))
            {
                var token = httpContext.Request.Headers["Authorization"].ToString();

                var jwtToken = token.Replace("Bearer ", string.Empty);

                var authSection = Configuration["Jwt:Key"]; // 4kadve15xfgs565hvdly53dsafd


                //******************************
                var tokenHandler = new JwtSecurityTokenHandler();

                var validationParameters = _parameters(authSection);
                tokenHandler.ValidateToken(jwtToken, validationParameters, out var validatedToken);

                if (!IsJwtWithValidSecurityAlgorithm(validatedToken)) /*return Task.FromResult(false)*/;

                var tokenPayload = tokenHandler.ReadJwtToken(jwtToken);

                var result = tokenPayload.Payload.TryGetValue("tokenTime", out var likeCardTokenExp);

                var x = Task.FromResult(result && ValidateTokenExpirationDate((long)likeCardTokenExp));

            };

            await _next(httpContext);
        }
        private readonly Func<string, TokenValidationParameters> _parameters = secret =>
            new TokenValidationParameters
            {
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret)),
                ValidateIssuerSigningKey = true,
                ValidateIssuer = false,
                ValidateAudience = false,
                RequireExpirationTime = false,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };

        private static bool IsJwtWithValidSecurityAlgorithm(SecurityToken validatedToken)
        {
            return validatedToken is JwtSecurityToken jwtSecurityToken &&
                   jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256,
                       StringComparison.InvariantCultureIgnoreCase);
        }

        private static bool ValidateTokenExpirationDate(long expTimestamp)
        {
            var tokenExpiryKsa = expTimestamp;
            var tokenExpiryDateTime = new DateTime(1970, 1, 1, 3, 0, 0).AddSeconds(tokenExpiryKsa);

            return tokenExpiryDateTime > DateTime.UtcNow;
        }

    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class CheckValidToken
    {
        public static IApplicationBuilder UseCheckValidToken(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CheckValidTokenMiddleWare>();
        }
    }
}

