using System.Reflection;
using System.Text;
using GameFreelancApp.Core.IConfiguration;
using GameFreelancApp.Extensions;
using GameFreelancApp.GameContext;
using GameFreelanceApp.BackgroundJobs;
using GameFreelanceApp.ExceptionHandlerMiddleWare;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Serilog;
using Serilog.Events;

//create the logger and setup your sinks, filters and properties
Log.Logger = new LoggerConfiguration()
    .WriteTo.File(path: Path.Combine("Logs", "log.txt"),
             outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}",
             rollingInterval: RollingInterval.Day,
             restrictedToMinimumLevel: LogEventLevel.Information
    ).CreateLogger();


var builder = WebApplication.CreateBuilder(args);

//after create the builder - UseSerilog

builder.Host.UseSerilog();

builder.Services.AddDbContext<GameDbContext>(optionsBuilder => optionsBuilder
                                                            .UseSqlServer(builder.Configuration.GetConnectionString("AppDb"),
                                                            sqlServerOptionsAction: sqlServerOptions =>
                                                            {
                                                                sqlServerOptions.CommandTimeout(60);
                                                                sqlServerOptions.EnableRetryOnFailure
                                                                (
                                                                    maxRetryCount: 5
                                                                );
                                                            }
                    ).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking), ServiceLifetime.Transient);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddHostedService<ResetScoresJob>();
builder.Services.AddSwaggerGen(options =>
{
    var titleBase = "SyStamp APP API";

    //Setting swagger authentications
    options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer",
        BearerFormat = "JWT",
        In = ParameterLocation.Header,
        Description = "JWT Authorization header using the Bearer scheme."
    });

    options.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                    new string[] { }
                }
    });
    // Set the comments path for the Swagger JSON and UI.
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    options.IncludeXmlComments(xmlPath);
});
//Add Microsoft.AspNetCore.Authentication.JwtBearer

builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(o =>
{
    o.TokenValidationParameters = new TokenValidationParameters
    {
        //ValidIssuer = builder.Configuration["Jwt:Issuer"],
        //ValidAudience = builder.Configuration["Jwt:Audience"],
        IssuerSigningKey = new SymmetricSecurityKey
        (Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"])),
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidateLifetime = false,
        ValidateIssuerSigningKey = true
    };
});

builder.Services.AddAutoMapper(typeof(MapperInit));
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware(typeof(ExceptionHandlerMiddleWare));
//app.UseMiddleware(typeof(CheckValidTokenMiddleWare));

app.UseStaticFiles();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
