﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameFreelanc.Response
{
    public class SuccessResponseDTO
    {
        public string Message { get; set; }
    }
    public class ErrorResponseDTO
    {
        public string Message { get; set; }
    }
}
