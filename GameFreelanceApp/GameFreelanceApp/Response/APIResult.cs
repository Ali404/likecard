﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameFreelanc.Response;

namespace GameFreelanc.Response
{
    public class APIResult<T> : BaseAPIResult
    {
        public T Data { get; set; }
    }
}
