﻿namespace GameFreelanceApp.Models
{
    public class PaidAttempts
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        public DateTime PurchaseDate { get; set; }

        public int AttemptsNum { get; set; }
    }
}
