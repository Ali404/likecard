﻿namespace GameFreelanceApp.Models
{
    public class DeductOperations
    {
        public int Id { get; set; }

        public long UserId { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;

        public string? Reference { get; set; }
    }
}
