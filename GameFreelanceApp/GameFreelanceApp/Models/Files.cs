﻿namespace GameFreelanceApp.Models
{
    public class Files
    {
        public int Id { get; set; }

        public string Path { get; set; }

        public int Order { get; set; }
    }
}
