﻿namespace GameFreelanceApp.Models
{
    public class SessionLink
    {
        public long Id { get; set; }

        public string? Link { get; set; }

        public string? EncryptedLink { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime ValidTo => CreatedAt.AddMinutes(15);

        public long UserId { get; set; }
    }
}
