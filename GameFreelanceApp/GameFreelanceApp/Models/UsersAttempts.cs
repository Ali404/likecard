﻿using GameFreelancApp.Enum;
using GameFreelanceApp.Enum;

namespace GameFreelancApp.Models
{
    public class UsersAttempts
    {
        public int Id { get; set; }

        public long UserId { get; set; }

        public DateTime Date { get; set; } = DateTime.UtcNow;

        public AttemptStatus Status { get; set; }

        public long Period { get; set; }

        public AttemptType Type { get; set; }

        public decimal Points { get; set; }

    }
}
