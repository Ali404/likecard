﻿using GameFreelancApp.Models;

namespace GameFreelanceApp.Models
{
    public class Users
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        public string UserName { get; set; }

        public virtual ICollection<SessionLink> SeseionLinks { get; set; } = new HashSet<SessionLink>();
        public virtual ICollection<UsersAttempts> UsersAttempts { get; set; } = new HashSet<UsersAttempts>();
        public virtual ICollection<PaidAttempts> PaidAttempts { get; set; } = new HashSet<PaidAttempts>();
        public virtual ICollection<DeductOperations> DeductOperations { get; set; } = new HashSet<DeductOperations>();
    }
}
