﻿namespace GameFreelancApp.Models
{
    public class GameSettings
    {
        public int Id { get; set; }

        public int FreeAttemptsPerDay { get; set; }

        public int PaidAttempts { get; set; }

        public int? AttemptPrice { get; set; }
    }
}
