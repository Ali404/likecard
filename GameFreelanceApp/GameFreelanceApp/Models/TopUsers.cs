﻿namespace GameFreelanceApp.Models
{
    public class TopUsers
    {
        public int Id { get; set; }

        public long UserId { get; set; }

        public string UserName { get; set; }

        public DateTime Date { get; set; }

    }
}
