﻿using System.Linq.Expressions;

namespace GameFreelancApp.Core.IRepositories
{
    public interface IGenericRepository<T> where T : class
    {
        public Task<List<T>> GetAll(Expression<Func<T, bool>> filter = null,
                                        Func<IQueryable<T>,
                                        IOrderedQueryable<T>> orderBy = null,
                                        string includeProperties = "");

        public Task<T> Get(Expression<Func<T, bool>> expression, string includeProperties = "");

        public Task<T> GetLast(Expression<Func<T, bool>> expression, Func<IQueryable<T>,
                                                IOrderedQueryable<T>> orderBy = null, string includeProperties = "");

        public Task Insert(T entity);
        public Task InsertRange(List<T> entity);

        public Task Delete(object id);

        public Task DeleteRange(List<T> entities);

        public Task Delete(T entityToDelete);

        public Task Update(T entityToUpdate);

        //public Task<PagedList<T>> GetAllWithPagination(PaginationParams paginationParams, Expression<Func<T, bool>> filter = null,
        //                                Func<IQueryable<T>,
        //                                IOrderedQueryable<T>> orderBy = null,
        //                                string includeProperties = "");
    }
}
