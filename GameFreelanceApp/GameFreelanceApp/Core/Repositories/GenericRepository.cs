﻿using System.Linq.Expressions;
using AutoMapper;
using GameFreelancApp.Core.IRepositories;
using GameFreelancApp.GameContext;
using Microsoft.EntityFrameworkCore;

namespace GameFreelancApp.Core.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly GameDbContext context;
        private readonly DbSet<T> dbSet;
        protected readonly IMapper _map;
        public GenericRepository(GameDbContext context, IMapper map)
        {
            this.context = context;
            this.dbSet = context.Set<T>();
            _map = map;
        }
        public async Task<List<T>> GetAll(Expression<Func<T, bool>> filter = null,
                                                Func<IQueryable<T>,
                                                IOrderedQueryable<T>> orderBy = null,
                                                string includeProperties = "")
        {
            IQueryable<T> query = dbSet;
            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync();
            }
            else
            {
                return await query.ToListAsync();
            }
        }
        public async Task<T> Get(Expression<Func<T, bool>> expression, string includeProperties = "")
        {
            IQueryable<T> query = dbSet;
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            return await query.AsNoTracking().FirstOrDefaultAsync(expression);
        }
        public async Task Insert(T entity)
        {
            await dbSet.AddAsync(entity);
        }
        public async Task Delete(object id)
        {
            T entityToDelete = await dbSet.FindAsync(id);
            dbSet.Remove(entityToDelete);

        }
        public async Task DeleteRange(List<T> entities)
        {
            dbSet.RemoveRange(entities);

        }
        public Task Delete(T entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
            return null;
        }
        public async Task Update(T entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;

        }
        public async Task InsertRange(List<T> entity)
        {
            await dbSet.AddRangeAsync(entity);
        }

        public async Task<T> GetLast(Expression<Func<T, bool>> expression, Func<IQueryable<T>,
                                                IOrderedQueryable<T>> orderBy = null, string includeProperties = "")
        {
            IQueryable<T> query = dbSet;

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            return await orderBy(query).LastOrDefaultAsync();

        }

        //    public Task<PagedList<T>> GetAllWithPagination(PaginationParams paginationParams, Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "")
        //    {
        //        IQueryable<T> query = dbSet;
        //        if (filter != null)
        //        {
        //            query = query.Where(filter);
        //        }

        //        foreach (var includeProperty in includeProperties.Split
        //            (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
        //        {
        //            query = query.Include(includeProperty);
        //        }

        //        if (orderBy != null)
        //        {
        //            query = orderBy(query);
        //        }

        //        return query.AsNoTracking().CreateAsync(paginationParams.PageNumber, paginationParams.PageSize);


        //    }
    }
}
