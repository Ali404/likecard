﻿using GameFreelancApp.Core.IRepositories;
using GameFreelancApp.Models;
using GameFreelanceApp.Models;

namespace GameFreelancApp.Core.IConfiguration
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<GameSettings> GameSettingsRepository { get; }
        IGenericRepository<UsersAttempts> UsersAttemptsRepository { get; }
        IGenericRepository<Users> UsersRepository { get; }
        IGenericRepository<SessionLink> SessionLinkRepository { get; }
        IGenericRepository<PaidAttempts> UsersPaidAttemptsRepository { get; }
        IGenericRepository<Files> FileRepository { get; }
        IGenericRepository<DeductOperations> DeductOperationsRepository { get; }

        Task Save();
    }
}
