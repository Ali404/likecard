﻿using AutoMapper;
using GameFreelancApp.Core.IRepositories;
using GameFreelancApp.Core.Repositories;
using GameFreelancApp.GameContext;
using GameFreelancApp.Models;
using GameFreelanceApp.Models;

namespace GameFreelancApp.Core.IConfiguration
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly GameDbContext _context;
        private readonly ILogger log;
        private readonly IMapper _map;
        public UnitOfWork(GameDbContext context, ILoggerFactory loggerFactory, IMapper map)
        {
            _context = context;
            log = loggerFactory.CreateLogger("logs");
            this._map = map;
        }

        private IGenericRepository<GameSettings> gameSettingsRepository;
        private IGenericRepository<UsersAttempts> usersAttemptsRepository;
        private IGenericRepository<SessionLink> sessionLinkRepository;
        private IGenericRepository<PaidAttempts> usersPaidAttemptsRepository;
        private IGenericRepository<Users> usersRepository;
        private IGenericRepository<Files> fileRepository;
        private IGenericRepository<DeductOperations> deductOperationsRepository;

        //------------------Check values---------------
        public IGenericRepository<GameSettings> GameSettingsRepository => gameSettingsRepository ??=
        new GenericRepository<GameSettings>(_context, _map);

        public IGenericRepository<UsersAttempts> UsersAttemptsRepository => usersAttemptsRepository ??=
        new GenericRepository<UsersAttempts>(_context, _map);

        public IGenericRepository<SessionLink> SessionLinkRepository => sessionLinkRepository ??=
        new GenericRepository<SessionLink>(_context, _map);

        public IGenericRepository<PaidAttempts> UsersPaidAttemptsRepository => usersPaidAttemptsRepository ??=
        new GenericRepository<PaidAttempts>(_context, _map);

        public IGenericRepository<Users> UsersRepository => usersRepository ??=
        new GenericRepository<Users>(_context, _map);

        public IGenericRepository<Files> FileRepository => fileRepository ??=
        new GenericRepository<Files>(_context, _map);

        public IGenericRepository<DeductOperations> DeductOperationsRepository => deductOperationsRepository ??=
        new GenericRepository<DeductOperations>(_context, _map);

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }

    }
}

