﻿using AutoMapper;
using GameFreelancApp.Dtos;
using GameFreelancApp.Models;

namespace GameFreelancApp.Extensions
{
    public class MapperInit : Profile
    {
        public MapperInit()
        {
            CreateMap<UsersAttempts, GetUserAttemptDto>();


        }
    }
}
