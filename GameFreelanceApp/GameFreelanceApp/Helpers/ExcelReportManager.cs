﻿using GameFreelanceApp.Dtos;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;

namespace GameFreelanceApp.Helpers
{
    public static class ExcelReportManager
    {
        private const string FontName = "Calibri";
        private const int FontHeight = 12;
        private const string ExcelFileExtension = ".xlsx";

        public static Task<ScoresReport> GenerateExcelReportAsync(string title, List<string> columnNames,
            List<List<string>> rows)
        {
            var workbook = new XSSFWorkbook();

            // Initialize report styles
            var style = InitializeStyle(workbook);

            // Initialize sheet
            var sheet = workbook.CreateSheet(title);

            // Row 0 - Create The Title Merged Cell
            CreateTitle(sheet, title, columnNames.Count);

            // Row 1 - Create The Header Row
            CreateRow(sheet, columnNames, 1, style);

            // Row 2 ~ Start data from 2
            var rowIndex = 2;
            foreach (var row in rows)
            {
                CreateRow(sheet, row, rowIndex++, style);
            }

            // Auto sized all the affected columns
            int lastColumnNum = sheet.GetRow(1).LastCellNum;
            for (var i = 0; i <= lastColumnNum; i++)
            {
                sheet.AutoSizeColumn(i);
                GC.Collect();
            }

            var report = new ScoresReport
            {
                FileName = title + ExcelFileExtension,
                ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            };

            using (var stream = new MemoryStream())
            {
                workbook.Write(stream);
                report.FileContents = stream.ToArray();
            }

            return Task.FromResult(report);
        }
        private static XSSFCellStyle InitializeStyle(XSSFWorkbook workbook)
        {
            var font = (XSSFFont)workbook.CreateFont();
            font.FontHeightInPoints = FontHeight;
            font.FontName = FontName;

            var style = (XSSFCellStyle)workbook.CreateCellStyle();
            style.SetFont(font);
            style.BorderLeft = BorderStyle.Medium;
            style.BorderTop = BorderStyle.Medium;
            style.BorderRight = BorderStyle.Medium;
            style.BorderBottom = BorderStyle.Medium;
            style.VerticalAlignment = VerticalAlignment.Center;
            return style;
        }

        private static void CreateRow(ISheet sheet, List<string> cols, int rowIndex, ICellStyle style)
        {
            var currentRow = sheet.CreateRow(rowIndex);

            style.BorderDiagonalLineStyle = BorderStyle.None;
            var font = sheet.Workbook.CreateFont();
            font.IsBold = false;
            for (var i = 0; i < cols.Count; i++)
            {
                CreateCell(currentRow, i, cols[i], style);
            }
        }

        private static void CreateTitle(ISheet sheet, string title, int colsNumber)
        {
            var style = sheet.Workbook.CreateCellStyle();

            var font = sheet.Workbook.CreateFont();
            font.FontHeightInPoints = 16;
            font.IsBold = true;
            font.FontName = FontName;

            style.SetFont(font);
            style.BorderLeft = BorderStyle.Medium;
            style.BorderTop = BorderStyle.Medium;
            style.BorderRight = BorderStyle.Medium;
            style.BorderBottom = BorderStyle.Medium;
            style.VerticalAlignment = VerticalAlignment.Center;
            style.Alignment = HorizontalAlignment.CenterSelection;

            if (colsNumber != 1)
                sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, colsNumber - 1));

            CreateCell(sheet.CreateRow(0), 0, title, style);

            sheet.AutoSizeColumn(0, true);
        }

        private static void CreateCell(IRow currentRow, int cellIndex, string value, ICellStyle style)
        {
            if (decimal.TryParse(value, out var number))
            {
                var cell = currentRow.CreateCell(cellIndex, CellType.Numeric);
                cell.SetCellValue((double)number);
                cell.CellStyle = style;
            }
            else
            {
                var cell = currentRow.CreateCell(cellIndex, CellType.String);
                cell.SetCellValue(value);
                cell.CellStyle = style;
            }
        }
    }
}

