﻿using System.Security.Cryptography;
using System.Text;

namespace GameFreelanceApp.Helpers.SessionLinkGenerator
{
    public static class Encrypt
    {
        public static string HashString(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return data;
            }
            using (var sha256 = SHA256.Create())
            {
                // Send a text to hash.  
                var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(data));

                // Get the hashed string.  
                return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            }
        }
        public static string EncryptData(string plainText)
        {
            try
            {
                string ToReturn = "";
                string publickey = "1AzO5GEe";
                string secretkey = "1AzO5GEe";
                byte[] secretkeyByte = { };
                secretkeyByte = System.Text.Encoding.UTF8.GetBytes(secretkey);
                byte[] publickeybyte = { };
                publickeybyte = System.Text.Encoding.UTF8.GetBytes(publickey);
                MemoryStream ms = null;
                CryptoStream cs = null;
                byte[] inputbyteArray = System.Text.Encoding.UTF8.GetBytes(plainText);
                using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
                {
                    ms = new MemoryStream();
                    cs = new CryptoStream(ms, des.CreateEncryptor(publickeybyte, secretkeyByte), CryptoStreamMode.Write);
                    cs.Write(inputbyteArray, 0, inputbyteArray.Length);
                    cs.FlushFinalBlock();
                    ToReturn = Convert.ToBase64String(ms.ToArray());
                }
                return ToReturn;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }
        public static string Decrypt(string encryptedData)
        {
            try
            {
                //string textToDecrypt = "VtbM/yjSA2Q=";
                string ToReturn = "";
                string publickey = "1AzO5GEe";
                string privatekey = "1AzO5GEe";
                byte[] privatekeyByte = { };
                privatekeyByte = System.Text.Encoding.UTF8.GetBytes(privatekey);
                byte[] publickeybyte = { };
                publickeybyte = System.Text.Encoding.UTF8.GetBytes(publickey);
                MemoryStream ms = null;
                CryptoStream cs = null;
                byte[] inputbyteArray = new byte[encryptedData.Replace(" ", "+").Length];
                inputbyteArray = Convert.FromBase64String(encryptedData.Replace(" ", "+"));
                using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
                {
                    ms = new MemoryStream();
                    cs = new CryptoStream(ms, des.CreateDecryptor(publickeybyte, privatekeyByte), CryptoStreamMode.Write);
                    cs.Write(inputbyteArray, 0, inputbyteArray.Length);
                    cs.FlushFinalBlock();
                    Encoding encoding = Encoding.UTF8;
                    ToReturn = encoding.GetString(ms.ToArray());
                }
                return ToReturn;
            }
            catch
            {
                throw new ApplicationException("This Link is not valid !!");
            }
        }
    }
}

