﻿using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace GameFreelanceApp.Helpers.SessionLinkGenerator
{
    public class SessionLinkGenerator
    {
        public static string RandomString(int length)
        {
            var random = new Random();
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
            .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string GenerateSessionLink(IConfiguration appSettings)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appSettings["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var link = new JwtSecurityToken(
                    issuer: appSettings["Issuer"],
                    signingCredentials: credentials
                                            );

            var toBeReturned = new JwtSecurityTokenHandler().WriteToken(link);
            return toBeReturned + Guid.NewGuid().ToString("N");
        }

    }
}
