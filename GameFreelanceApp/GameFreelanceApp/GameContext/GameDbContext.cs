﻿using GameFreelancApp.Models;
using GameFreelanceApp.Models;
using Microsoft.EntityFrameworkCore;

namespace GameFreelancApp.GameContext
{
    public class GameDbContext : DbContext
    {
        public GameDbContext(DbContextOptions<GameDbContext> options) : base(options)
        {
        }

        public virtual DbSet<GameSettings> GameSettings { get; set; }
        public virtual DbSet<UsersAttempts> UsersAttempts { get; set; }
        public virtual DbSet<PaidAttempts> PaidAttempts { get; set; }
        public virtual DbSet<SessionLink> SessionLinks { get; set; }
        public virtual DbSet<DeductOperations> DeductOperations { get; set; }
        public virtual DbSet<Files> Files { get; set; }
        public virtual DbSet<Users> Users { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

        }
    }
}
