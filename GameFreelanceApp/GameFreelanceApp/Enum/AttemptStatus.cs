﻿namespace GameFreelancApp.Enum
{
    public enum AttemptStatus : byte
    {
        Failed = 1,
        Passed = 2
    }
}
