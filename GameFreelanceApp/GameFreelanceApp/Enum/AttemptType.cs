﻿namespace GameFreelanceApp.Enum
{
    public enum AttemptType : byte
    {
        Free = 1,
        Paid = 2
    }
}
