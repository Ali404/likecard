﻿using AutoMapper;
using GameFreelanc.Response;
using GameFreelancApp.Core.IConfiguration;
using GameFreelancApp.Dtos;
using GameFreelancApp.Models;
using GameFreelanceApp.Controllers;
using GameFreelanceApp.Dtos;
using GameFreelanceApp.Helpers;
using GameFreelanceApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace GameFreelancApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly IMapper _map;

        public GameController(IUnitOfWork unitOfWork, IMapper map, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _configuration = configuration;
            _map = map;
        }

        /// <summary>
        /// Create or update settings
        /// </summary>
        [HttpPost("CreateOrUpdateSettings")]
        [ProducesResponseType(statusCode: 200, Type = typeof(APIResult<GameSettings>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(BaseAPIResult))]
        public async Task<APIResult<GameSettings>> CreateSettingsAsync([FromBody] CreateOrUpdateSettingsDto input)
        {
            var settings = await _unitOfWork.GameSettingsRepository.Get(x => x.Id != 0);
            var settingsToBeAdded = new GameSettings();
            var result = new GameSettings();

            if (settings == null)
            {
                settingsToBeAdded.FreeAttemptsPerDay = input.FreeAttemptsPerDay;
                settingsToBeAdded.AttemptPrice = input.AttemptPrice;
                settingsToBeAdded.PaidAttempts = input.PaidAttempts;

                await _unitOfWork.GameSettingsRepository.Insert(settingsToBeAdded);
                await _unitOfWork.Save();

                result = _map.Map<GameSettings>(settingsToBeAdded);
                return SuccessResponse(result);
            }

            settings.FreeAttemptsPerDay = input.FreeAttemptsPerDay;
            settings.AttemptPrice = input.AttemptPrice;
            settings.PaidAttempts = input.PaidAttempts;

            await _unitOfWork.GameSettingsRepository.Update(settings);
            await _unitOfWork.Save();

            result = _map.Map<GameSettings>(settings);
            return SuccessResponse(result);
        }

        /// <summary>
        /// Get Game settings.
        /// </summary>
        [HttpGet("GetSettings")]
        [ProducesResponseType(statusCode: 200, Type = typeof(APIResult<GameSettings>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(BaseAPIResult))]
        public async Task<APIResult<GameSettings>> GetSettingsAsync()
        {
            var query = await _unitOfWork.GameSettingsRepository.GetAll();

            return SuccessResponse(query.FirstOrDefault());
        }

        /// <summary>
        /// Generate excel report for players.
        /// </summary>
        [HttpPost("GenerateReport")]
        [ProducesResponseType(statusCode: 200, Type = typeof(APIResult<>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(BaseAPIResult))]
        public async Task<ActionResult> GenerateReportAsync()
        {
            var list = await _unitOfWork.UsersAttemptsRepository.GetAll();

            if (list.Count == 0)
                return Ok();
            //throw new BusinessException(_localizer["EmptyList"]);

            var headers = new List<string>
                {
                    "UserId",
                    "UserName",
                    "Status",
                    "Points",
                    "Type"
                };

            var rows = new List<List<string>>();

            var users = await _unitOfWork.UsersRepository.GetAll(x => list.Select(x => x.UserId).Distinct().ToList()
                                                         .Contains(x.UserId));
            foreach (var item in list)
            {
                var userName = users.Where(x => x.UserId == item.UserId).Select(x => x.UserName).FirstOrDefault();

                rows.Add(new List<string>
                {
                    item.UserId.ToString(),
                    userName,
                    item.Status.ToString(),
                   // item.Score.ToString(),
                    item.Points.ToString(),
                    item.Type.ToString()
                });
            }

            var report = await ExcelReportManager.GenerateExcelReportAsync("ScoreResult", headers, rows);

            report.FileName = "ScoreResult";

            return File(report.FileContents, report.ContentType, report.FileName);
        }


        /// <summary>
        /// Upload image.
        /// </summary>
        [HttpPost("UploadFile")]
        [ProducesResponseType(statusCode: 200, Type = typeof(APIResult<List<FilesDto>>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(BaseAPIResult))]
        public async Task<APIResult<List<FilesDto>>> UploadFileAsync([FromForm] UploadFileDto input)
        {
            if (input.Userfile == null)
                throw new ApplicationException("");

            var fileName = Path.GetFileName(input.Userfile.FileName);
            string filePath = Path.Combine(Directory.GetCurrentDirectory(), _configuration["ContentFolder"], fileName);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await input.Userfile.CopyToAsync(fileStream);
            }

            var toBeInserted = new Files
            {
                Path = $"{_configuration["ContentFolder"]}{fileName}",
                Order = input.Order
            };

            await _unitOfWork.FileRepository.Insert(toBeInserted);
            await _unitOfWork.Save();

            var data = await _unitOfWork.FileRepository.GetAll();

            var result = data?.ConvertAll(x => new FilesDto
            {
                Id = x.Id,
                Url = _configuration["ContentUrl"] + x.Path,
                Order = x.Order
            });

            return SuccessResponse(result);
        }


        /// <summary>
        /// Get uploaded files.
        /// </summary>
        [HttpGet("GetFiles")]
        [ProducesResponseType(statusCode: 200, Type = typeof(APIResult<List<FilesDto>>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(BaseAPIResult))]
        public async Task<APIResult<List<FilesDto>>> GetFilesAsync()
        {
            var data = await _unitOfWork.FileRepository.GetAll();

            var result = data?.ConvertAll(x => new FilesDto
            {
                Id = x.Id,
                Url = _configuration["ContentUrl"] + x.Path,
                Order = x.Order
            });

            return SuccessResponse(result);
        }

        /// <summary>
        /// delete file.
        /// </summary>
        [HttpDelete("DeleteFile")]
        [ProducesResponseType(statusCode: 200, Type = typeof(APIResult<>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(BaseAPIResult))]
        public async Task<ActionResult> DeleteFileAsync([FromQuery] int id)
        {
            var data = await _unitOfWork.FileRepository.Get(x => x.Id == id);
            if (data == null)
                throw new ApplicationException($"there is no file with this Id {id}");
            await _unitOfWork.FileRepository.Delete(id);
            await _unitOfWork.Save();

            return Ok();
        }

        /// <summary>
        /// Get users data.
        /// </summary>
        [HttpGet("GetUsersData")]
        [ProducesResponseType(statusCode: 200, Type = typeof(APIResult<List<PlayersDto>>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(BaseAPIResult))]
        public async Task<APIResult<List<PlayersDto>>> GetUsersDataAsync()
        {
            var result = new List<PlayersDto>();

            var list = await _unitOfWork.UsersAttemptsRepository.GetAll();

            if (list.Count == 0)
                return null;

            var usersIds = list.Select(x => x.UserId).Distinct().ToList();

            var users = await _unitOfWork.UsersRepository.GetAll(x => usersIds.Contains(x.UserId));

            foreach (var item in list)
            {
                result.Add(new PlayersDto
                {
                    UserId = (int)item.UserId,
                    UserName = users.Where(x => x.UserId == item.UserId).Select(x => x.UserName).FirstOrDefault(),
                    Points = item.Points,
                    Status = item.Status.ToString(),
                    Type = item.Type.ToString(),
                    Date = item.Date.ToString("dddd, dd MMMM yyyy")
                });
            }

            return SuccessResponse(result);
        }
    }
}
