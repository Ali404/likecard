﻿using System.Net;
using AutoMapper;
using GameFreelanc.Response;
using GameFreelancApp.Core.IConfiguration;
using GameFreelancApp.Dtos;
using GameFreelancApp.Models;
using GameFreelanceApp.Controllers;
using GameFreelanceApp.Dtos;
using GameFreelanceApp.Helpers.SessionLinkGenerator;
using GameFreelanceApp.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace GameFreelancApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttemptsController : BaseController
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly IMapper _map;


        public AttemptsController(IUnitOfWork unitOfWork, IMapper map, IConfiguration configuration)
        {
            _configuration = configuration;
            _unitOfWork = unitOfWork;
            _map = map;
        }

        /// <summary>
        /// Create new attempt.
        /// </summary>
        [HttpPost("CreateAttemptAsync")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ProducesResponseType(statusCode: 200, Type = typeof(APIResult<GetUserAttemptDto>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(BaseAPIResult))]
        public async Task<APIResult<GetUserAttemptDto>> CreateAttemptAsync([FromBody] CreateUserAttemptDto input)
        {
            var userId = 1;// long.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var userName = "";// User.FindFirst(ClaimTypes.Name == "userName")?.Value;

            //check if the link is active
            var link = await _unitOfWork.SessionLinkRepository.GetLast(x => x.Link == Encrypt.Decrypt(input.Link), orderBy: x => x.OrderByDescending(x => x.Id));
            if (link == null || link.ValidTo < DateTime.Now)
            {
                throw new ApplicationException("sorry!! The session is over");
            }

            var user = await _unitOfWork.UsersRepository.Get(x => x.Id == userId);
            if (user == null)
            {
                throw new ApplicationException("sorry!! There is no registered user with this link");
            }

            //get game settings : We should have only one row
            var gameSettings = await _unitOfWork.GameSettingsRepository.Get(x => x.Id != 0);

            //get free attempts count per day
            var freeAttemptsCountSetting = gameSettings?.FreeAttemptsPerDay;

            //get free attempts count for user
            var freeUserAttemptsCount = await _unitOfWork.UsersAttemptsRepository.GetAll(x => x.UserId == userId && x.Type == GameFreelanceApp.Enum.AttemptType.Free && x.Date.Date == DateTime.Today);

            input.Type = freeUserAttemptsCount.Count() == freeAttemptsCountSetting ?
                input.Type = GameFreelanceApp.Enum.AttemptType.Paid : input.Type = GameFreelanceApp.Enum.AttemptType.Free;

            var attempToBeAdded = new UsersAttempts
            {
                UserId = userId,
                Status = input.Status,
                Period = input.Period,
                Type = input.Type,
                Date = DateTime.Now,
                Points = input.Points,
                //Score = 60000 / input.Period * 1
            };

            await _unitOfWork.UsersAttemptsRepository.Insert(attempToBeAdded);
            await _unitOfWork.Save();

            var result = _map.Map<GetUserAttemptDto>(attempToBeAdded);
            return SuccessResponse(result);
        }

        /// <summary>
        /// Get link to enter new session
        /// </summary>
        [HttpGet("GetNewSession")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ProducesResponseType(statusCode: 200, Type = typeof(APIResult<SessionLinkDto>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(BaseAPIResult))]
        public async Task<APIResult<SessionLinkDto>> GetNewSessionAsync()
        {
            bool hasFreeAttempts = true;

            var userId = 1;// long.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var userName = "";// User.FindFirst(ClaimTypes.Name == "userName")?.Value;

            //check if there is active link
            var link = await _unitOfWork.SessionLinkRepository.GetLast(x => x.UserId == userId, orderBy: x => x.OrderByDescending(x => x.Id));
            if (link != null && link.ValidTo >= DateTime.Now)
                return SuccessResponse(new SessionLinkDto { Link = link.EncryptedLink });

            //get game settings : We should have only one row
            var gameSettings = await _unitOfWork.GameSettingsRepository.Get(x => x.Id != 0);

            //get free attempts count per day
            var freeAttemptsCountSetting = gameSettings?.FreeAttemptsPerDay;

            //get free attempts count for user
            var freeUserAttemptsCount = await _unitOfWork.UsersAttemptsRepository.GetAll(x => x.UserId == userId && x.Type == GameFreelanceApp.Enum.AttemptType.Free && x.Date.Date == DateTime.Today);

            if (freeUserAttemptsCount.Count() == freeAttemptsCountSetting)
                hasFreeAttempts = false;

            //check if he has paid attempts
            var userPaidAttempts = await _unitOfWork.UsersPaidAttemptsRepository.GetLast(x => x.UserId == userId, orderBy: x => x.OrderByDescending(x => x.PurchaseDate));

            if (userPaidAttempts == null && !hasFreeAttempts)
            {
                throw new ApplicationException("sorry!! you need to buy new attempts");
            }
            if (userPaidAttempts == null)
            {
                var numOfPaidAttempts = userPaidAttempts?.AttemptsNum;
                var purchseDate = userPaidAttempts?.PurchaseDate;

                var paidAttemptsCount = await _unitOfWork.UsersAttemptsRepository.GetAll(x => x.UserId == userId && x.Date >= purchseDate && x.Type == GameFreelanceApp.Enum.AttemptType.Paid);

                if (numOfPaidAttempts == paidAttemptsCount.Count)
                {
                    throw new ApplicationException("sorry!! Your Paid attempts are over, you need to buy new attempts");
                }
            }

            var queryDto = new BalanceQuery
            {
                UserId = 1558267323//int.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value)
            };

            //Get balance : integrate with main app => need refactoring

            var client = new HttpClient();
            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri("https://taxes.like4app.com/s2s/lc/inquiry"),
                Headers =
                {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiIxNTU4MjY3MzIzIiwibmV3RGV2aWNlSWQiOiIiLCJ1c2VybmFtZSI6IkFobWFkIiwic3RvcmVJZCI6IjIiLCJ0b2tlblRpbWUiOjE2NjIzNjEyNzV9.EiWYPx_WquwFVKrjasoT_OFyz84iTnfJaSu3s-OMem0" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" },
                    { "apiKey", "7cedad159fc21cd289e2d1bf3aa406849e166224070a4ee732cac72e483cfc9d" }
                },
                //Content = new StringContent(JsonConvert.SerializeObject(queryDto.UserId))
            };

            var content = new MultipartFormDataContent();
            content.Add(new StringContent(queryDto.UserId.ToString()), "userId");
            httpRequestMessage.Content = content;

            var response = await client.SendAsync(httpRequestMessage);

            var apiResponse = await response.Content.ReadAsStringAsync();
            var responseContent = JsonConvert.DeserializeObject<InqueryResponseDto>(apiResponse);

            //if (responseContent?.Data == null || responseContent?.Data?.Lc == 0)
            //{
            //    throw new ApplicationException("sorry!! you need to buy new attempts");
            //}

            var newLink = SessionLinkGenerator.GenerateSessionLink(_configuration);
            var encryptedLink = Encrypt.EncryptData(newLink);

            var user = await _unitOfWork.UsersRepository.Get(x => x.Id == userId);
            if (user == null)
            {
                var toBeEntered = new Users
                {
                    UserId = userId,
                    UserName = userName
                };
                await _unitOfWork.UsersRepository.Insert(toBeEntered);
                await _unitOfWork.Save();
            }

            var sessionLink = new SessionLink
            {
                Link = newLink,
                EncryptedLink = encryptedLink,
                CreatedAt = DateTime.Now,
                UserId = userId
            };

            await _unitOfWork.SessionLinkRepository.Insert(sessionLink);
            await _unitOfWork.Save();

            return SuccessResponse(new SessionLinkDto { Link = encryptedLink });
        }

        /// <summary>
        /// Get top users.
        /// </summary>
        [HttpGet("GetTopUsers")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ProducesResponseType(statusCode: 200, Type = typeof(APIResult<List<WinnerUserDto>>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(BaseAPIResult))]
        public async Task<APIResult<List<WinnerUserDto>>> GetTopUsersAsync([FromQuery] TopUsersQueryDto input)
        {
            var result = new List<WinnerUserDto>();

            input.To ??= DateTime.Today.AddDays(-1);
            input.From ??= DateTime.Today.AddDays(-7);

            var usersAttempts = await _unitOfWork.UsersAttemptsRepository.GetAll(x => x.Date <= input.To && x.Date >= input.From,
                                                                        null, "");

            var users = await _unitOfWork.UsersRepository.GetAll(x => usersAttempts.Select(x => x.UserId).Distinct().ToList()
                                             .Contains(x.UserId));

            var groupedValue = usersAttempts.GroupBy(x => x.UserId).ToDictionary(x => x.Key, c => c.ToList());
            foreach (var item in groupedValue)
            {
                var userName = users.Where(x => x.UserId == item.Key).Select(x => x.UserName).FirstOrDefault();

                //var y = 60000 / item.Value.Sum(x => x.Period) * item.Value.Count;
                var points = item.Value.Sum(x => x.Points);

                result.Add(new WinnerUserDto
                {
                    Id = item.Key,
                    Points = points,
                    UserName = userName
                });
            }
            return SuccessResponse(result);
        }

        /// <summary>
        /// Buy attempts.
        /// </summary>
        [HttpPost("BuyAttempts")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ProducesResponseType(statusCode: 200, Type = typeof(APIResult<>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(BaseAPIResult))]
        public async Task<ActionResult> BuyAttemptsAsync()
        {
            var userId = 1558267323;// long.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            //Deduct balance
            var client = new HttpClient();
            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri("https://taxes.like4app.com/s2s/lc/deduct"),
                Headers =
                {
                    { HttpRequestHeader.Authorization.ToString(), "Bearer xxxxxxxxxxxxxxxxxxx" },
                    { HttpRequestHeader.Accept.ToString(), "application/json" },
                    { "apiKey", "7cedad159fc21cd289e2d1bf3aa406849e166224070a4ee732cac72e483cfc9d" }
                },
                Content = new StringContent(JsonConvert.SerializeObject(1))
            };
            var attemptsToBuy = 0;

            var gameSettings = await _unitOfWork.GameSettingsRepository.Get(x => x.Id != 0);
            if (gameSettings != null)
            {
                attemptsToBuy = gameSettings.PaidAttempts;
            }

            var serviceRef = Guid.NewGuid().ToString("N");

            var content = new MultipartFormDataContent();
            content.Add(new StringContent(userId.ToString()), "userId");
            content.Add(new StringContent(attemptsToBuy.ToString()), "amount");
            content.Add(new StringContent("1"), "statusId");
            content.Add(new StringContent("خصم نتيجة ربح"), "descriptionAr");
            content.Add(new StringContent(""), "descriptionEn");
            content.Add(new StringContent(serviceRef), "serviceRef");

            httpRequestMessage.Content = content;

            var response = await client.SendAsync(httpRequestMessage);

            var apiResponse = await response.Content.ReadAsStringAsync();
            var responseContent = JsonConvert.DeserializeObject<DeductResponseDto>(apiResponse);

            if (responseContent?.Data.Length == 0)
            {
                throw new ApplicationException("Sorry !!, you don't have enough Lc");
            }

            var newPaidToBeEntered = new PaidAttempts
            {
                PurchaseDate = DateTime.Now,
                UserId = userId,
                AttemptsNum = attemptsToBuy
            };

            await _unitOfWork.UsersPaidAttemptsRepository.Insert(newPaidToBeEntered);

            var deductOperationsToBeEntered = new DeductOperations
            {
                UserId = userId,
                Date = DateTime.Now,
                Reference = serviceRef
            };

            await _unitOfWork.DeductOperationsRepository.Insert(deductOperationsToBeEntered);
            await _unitOfWork.Save();

            return Ok();
        }

        /// <summary>
        /// Get user data.
        /// </summary>
        [HttpGet("GetUserData")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ProducesResponseType(statusCode: 200, Type = typeof(APIResult<UserInfoDto>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(BaseAPIResult))]
        public async Task<APIResult<UserInfoDto>> GetUserDataAsync()
        {
            var userId = 1;//long.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);

            var data = await _unitOfWork.UsersAttemptsRepository.GetAll(x => x.Id == userId, null, "");

            var user = await _unitOfWork.UsersRepository.Get(x => x.UserId == userId);

            var tobeReturned = new UserInfoDto
            {
                UserId = userId,
                UserName = user.UserName,
                Points = data.Sum(x => x.Points)
            };

            //get game settings : We should have only one row
            var gameSettings = await _unitOfWork.GameSettingsRepository.Get(x => x.Id != 0);

            //get free attempts count per day
            var freeAttemptsCountSetting = gameSettings == null ? 0 : gameSettings.FreeAttemptsPerDay;

            //get free attempts count for user
            var userAttemptsCount = await _unitOfWork.UsersAttemptsRepository.GetAll(x => x.UserId == userId && x.Date.Date == DateTime.Today);

            //check if he has paid attempts
            var userPaidAttempts = await _unitOfWork.UsersPaidAttemptsRepository.GetLast(x => x.UserId == userId, orderBy: x => x.OrderByDescending(x => x.PurchaseDate));

            tobeReturned.AttemptsNum = (int)((freeAttemptsCountSetting + (userPaidAttempts == null ? 0 : userPaidAttempts.AttemptsNum)) - userAttemptsCount?.Count);
            tobeReturned.UserAttemptsCount = userAttemptsCount.Count;

            return SuccessResponse(tobeReturned);
        }
    }
}
