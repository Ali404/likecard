﻿using GameFreelanc.Response;
using Microsoft.AspNetCore.Mvc;

namespace GameFreelanceApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {

        public BaseController()
        {
        }

        /// <summary>
        /// Get Service
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected T GetService<T>() where T : class
        {
            // using Microsoft.Extensions.DependencyInjection;
            return HttpContext.RequestServices.GetService<T>();
        }



        /// <summary>
        /// return error response from model state
        /// </summary>
        /// <returns></returns>
        [NonAction]
        protected BaseAPIResult ErrorResponseFromModelState()
        {
            return new BaseAPIResult
            {
                IsSuccess = false,
                Message = ModelState.Values.FirstOrDefault(v => v.Errors.Any())?.Errors?.FirstOrDefault()?.ErrorMessage ?? "Un Expected Error"
            };
        }

        protected BaseAPIResult BaseSuccessResponse(string message = null)
        {
            return new BaseAPIResult
            {
                IsSuccess = true,
                Message = message ?? "Task Completed Successfully"
            };
        }

        protected BaseAPIResult ErrorResponse(string message = null)
        {
            return new BaseAPIResult
            {
                IsSuccess = false,
                Message = message ?? "Un Expected Error"
            };
        }

        protected BaseAPIResult ErrorResponse(int code, string message = null)
        {
            return new APIResult<object>
            {
                IsSuccess = false,
                Code = code,
                Message = message ?? "Un Expected Error"
            };
        }

        protected BaseAPIResult ErrorResponse<T>(int code, T data, string message = null)
        {
            return new APIResult<T>
            {
                IsSuccess = false,
                Code = code,
                Data = data,
                Message = message ?? "Un Expected Error"
            };
        }

        protected APIResult<T> SuccessResponse<T>(T data, string message = null)
        {
            return new APIResult<T>
            {
                IsSuccess = true,
                Message = message ?? "Task Completed Successfully",
                Data = data
            };
        }
    }
}
