﻿namespace GameFreelanceApp.Dtos
{
    public class PlayersDto
    {
        public int UserId { get; set; }

        public string UserName { get; set; }

        public decimal Points { get; set; }

        public string Status { get; set; }

        public string Type { get; set; }

        public string Date { get; set; }
    }
}
