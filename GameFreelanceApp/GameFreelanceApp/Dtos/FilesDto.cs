﻿namespace GameFreelanceApp.Dtos
{
    public class FilesDto
    {
        public int Id { get; set; }

        public string Url { get; set; }

        public int Order { get; set; }
    }
}
