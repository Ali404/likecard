﻿using GameFreelancApp.Enum;
using GameFreelanceApp.Enum;

namespace GameFreelancApp.Dtos
{
    public class GetUserAttemptDto
    {
        public int Id { get; set; }

        public long UserId { get; set; }

        public string UserName { get; set; }

        public AttemptStatus Status { get; set; }

        public AttemptType Type { get; set; }

        public DateTime Date { get; set; }

        public long Period { get; set; }

        public decimal CurrentScore { get; set; }

        public int Points { get; set; }
    }
}
