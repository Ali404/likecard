﻿namespace GameFreelanceApp.Dtos
{
    public class SessionLinkDto
    {
        public string Link { get; set; }
    }
}
