﻿namespace GameFreelanceApp.Dtos
{
    public class ScoresReport
    {
        public string FileName { get; set; }

        public string ContentType { get; set; }

        public byte[] FileContents { get; set; }
    }
}
