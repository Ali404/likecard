﻿namespace GameFreelanceApp.Dtos
{
    public class UserInfoDto
    {
        public long UserId { get; set; }

        public string UserName { get; set; }

        public decimal Points { get; set; }

        public int AttemptsNum { get; set; }

        public int UserAttemptsCount { get; set; }
    }
}
