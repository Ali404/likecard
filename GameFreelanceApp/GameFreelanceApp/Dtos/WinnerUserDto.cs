﻿namespace GameFreelanceApp.Dtos
{
    public class WinnerUserDto
    {
        public long Id { get; set; }

        public string? UserName { get; set; }

        public decimal Points { get; set; }
    }
}
