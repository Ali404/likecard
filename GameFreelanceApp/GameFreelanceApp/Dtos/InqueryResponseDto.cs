﻿namespace GameFreelanceApp.Dtos
{
    public class InqueryResponseDto
    {
        public int Status { get; set; }

        public string? Message { get; set; }

        public Responsedata Data { get; set; }
    }

    public class Responsedata
    {
        public decimal Lc { get; set; }
    }
}
