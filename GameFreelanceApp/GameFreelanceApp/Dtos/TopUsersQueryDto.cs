﻿namespace GameFreelanceApp.Dtos
{
    public class TopUsersQueryDto
    {
        public DateTime? From { get; set; }

        public DateTime? To { get; set; }
    }
}
