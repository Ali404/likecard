﻿using System.ComponentModel.DataAnnotations;
using GameFreelancApp.Enum;
using GameFreelanceApp.Enum;

namespace GameFreelancApp.Dtos
{
    public class CreateUserAttemptDto
    {
        [Required]
        public AttemptStatus Status { get; set; }

        [Required]
        public long Period { get; set; }

        [Required]
        public string Link { get; set; }

        public AttemptType Type { get; set; }

        [Required]
        public int Points { get; set; }
    }
}
