﻿namespace GameFreelancApp.Dtos
{
    public class GetSettingsDto
    {
        public int Id { get; set; }

        public int FreeAttempts { get; set; }

        public int PaidAttempts { get; set; }
    }
}
