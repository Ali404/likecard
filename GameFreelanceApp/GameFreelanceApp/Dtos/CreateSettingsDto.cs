﻿namespace GameFreelancApp.Dtos
{
    public class CreateOrUpdateSettingsDto
    {
        public int FreeAttemptsPerDay { get; set; }

        public int PaidAttempts { get; set; }

        public int? AttemptPrice { get; set; }
    }
}
