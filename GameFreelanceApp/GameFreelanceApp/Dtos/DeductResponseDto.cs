﻿namespace GameFreelanceApp.Dtos
{
    public class DeductResponseDto
    {
        public int Status { get; set; }

        public string? Message { get; set; }

        public DeductResponsedata[] Data { get; set; }
    }

    public class DeductResponsedata
    {

    }
}