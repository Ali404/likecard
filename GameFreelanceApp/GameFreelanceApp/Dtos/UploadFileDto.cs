﻿namespace GameFreelanceApp.Dtos
{
    public class UploadFileDto
    {
        public IFormFile Userfile { get; set; }

        public int Order { get; set; }
    }
}
